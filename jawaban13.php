INSERT INTO customers (name, email, password)
VALUES ('John Doe', 'john@doe.com', 'john123');

INSERT INTO customers (name, email, password)
VALUES ('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO orders (amount, customer_id)
VALUES ('500', '1');

INSERT INTO orders (amount, customer_id)
VALUES ('200', '2');

INSERT INTO orders (amount, customer_id)
VALUES ('750', '2');

INSERT INTO orders (amount, customer_id)
VALUES ('250', '1');

INSERT INTO orders (amount, customer_id)
VALUES ('400', '2');