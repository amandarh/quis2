SELECT customers.name AS customer_name, SUM(orders.amount)
FROM orders
INNER JOIN customers
ON customers.id = orders.customer_id
GROUP BY customer_id